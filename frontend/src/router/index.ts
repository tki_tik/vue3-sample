import { createRouter, createWebHistory } from 'vue-router';
import Menu1 from '~/pages/Menu1.vue';
import Menu2 from '~/pages/Menu2.vue';
import Home from '~/pages/Home.vue';
import NotFound from '~/pages/NotFound.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', name: 'ホーム', component: Home },
    { path: '/menu1', name: 'メニュー１', component: Menu1 },
    { path: '/menu2', name: 'メニュー２', component: Menu2 },
    { path: '/:catchAll(.*)', component: NotFound },
  ],
});

export default router;
