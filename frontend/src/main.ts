import { createPinia } from 'pinia';
import { createApp } from 'vue';
import HistoryStatePlugin from 'vue-history-state';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faHome } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import './assets/tailwind.css';
import router from './router';
import App from './App.vue';

library.add(faBars, faHome);

const app = createApp(App);

app.use(router);
app.use(createPinia());
app.use(HistoryStatePlugin, {});
app.component('fa', FontAwesomeIcon);

app.mount('#app');
