import { defineStore } from 'pinia';

export const useGlobalStore = defineStore('global', {
  state: () => {
    return {
      isOpenSideNavi: true,
      isHoverSideNavi: false,
    };
  },
  actions: {
    toggleSideNavi() {
      this.isOpenSideNavi = !this.isOpenSideNavi;
    },
  },
});
